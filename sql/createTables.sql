CREATE TABLE users (
    id       SERIAL PRIMARY KEY,
    username varchar(50) NOT NULL UNIQUE,
    email    varchar(100) NOT NULL UNIQUE,
    password_hash  varchar(255) NOT NULL
    CONSTRAINT proper_email CHECK (email ~* '^[A-Za-z0-9._+%-]+@[A-Za-z0-9.-]+[.][A-Za-z]+$')
);

CREATE TABLE boards (
    id SERIAL PRIMARY KEY,
    owner INT NOT NULL REFERENCES users(id),
    created TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT CURRENT_TIMESTAMP,
    last_modified TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT CURRENT_TIMESTAMP,
    CHECK (created <= last_modified)
);

CREATE TABLE notes (
   id SERIAL PRIMARY KEY,
   board INT NOT NULL REFERENCES boards(id),
   title VARCHAR(50) NOT NULL,
   content TEXT NOT NULL CHECK (LENGTH(content) <= 1000),
   created TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT CURRENT_TIMESTAMP,
   last_modified TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT CURRENT_TIMESTAMP,
   position INT NOT NULL,
   color_id SMALLINT NOT NULL DEFAULT 0
       CHECK (notes.color_id >= 0 AND notes.color_id <= 9),
   CONSTRAINT no_duplicate_note UNIQUE (board, position)
);
