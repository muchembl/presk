class Note{
    #id;
    #board;
    #created;
    #lastModified;
    #position;
    #color;
    #title;
    #content;
    #titleInputListeners = [];
    #contentInputListeners = [];
    #colorInputListeners = [];

    constructor(id, board, created, lastModified, position, color, title, content) {
        this.#id = id;
        this.#board = board;
        this.#created = created;
        this.#lastModified = lastModified;
        this.#position = position;
        this.#color = color;
        this.#title = title;
        this.#content = content;
    }

    position(){
        return this.#position;
    }
    color(){
        return this.#color;
    }
    title(){
        return this.#title;
    }
    content(){
        return this.#content;
    }

    setPosition(position){
        this.#position = position;
    }
    setColor(color){
        this.#color = color;
        for(let i = 0; i < this.#colorInputListeners.length; i++){
            this.#colorInputListeners[i](this);
        }
    }
    setTitle(title) {
        this.#title = title;
        for(let i = 0; i < this.#titleInputListeners.length; i++){
            this.#titleInputListeners[i](this);
        }
    }
    setContent(content){
        this.#content = content;
        for(let i = 0; i < this.#contentInputListeners.length; i++){
            this.#contentInputListeners[i](this);
        }
    }

    setId(id){
        this.#id = id;
    }

    setCreated(created){
        this.#created = created;
    }

    setLastModified(lastModified){
        this.#lastModified = lastModified;
        this.save();
    }

    createView(){
        const view= this.#createCardView();
        view.appendChild(this.#createTitleView());
        view.appendChild(this.#createContentView());
        return view;
    }

    #createTitleView(){
        const title = document.createElement("input");
        title.value = this.#title;
        title.id = this.titleId();
        title.className = "note-title color-"+this.#color;

        title.maxLength = 50;
        title.spellcheck = false;

        title.oninput = (ev) => {
            this.setTitle(title.value);
            updateToolBox(this);
        }

        title.addEventListener("focusout", (ev) => {
            title.setSelectionRange(0,0);
            this.save()
        });

        this.addTitleListener((note) => {
            title.value = note.title();
        });

        this.addColorListener((note) => {
            title.className = "note-title color-"+ note.#color;
        });

        return title;
    }


    #createContentView(){
        const content = document.createElement("p");
        let html = contentToHtml(this.#content);

        content.innerHTML = html;
        content.contentEditable = "true";
        content.style.userSelect = "text";
        content.id = this.contentId();
        content.className = "note-content";
        content.spellcheck= false;


        content.addEventListener("focusout", (ev) => {

            this.save();
        });


        function selectionChecker(action){
            let pos = getSelectionPos();
            const v = pos[0];
            const sP = pos[1];
            const eP = pos[2];

            const content = ""+v.innerHTML;
            const boldIntervals = intervals(content, 'b');
            const italicIntervals = intervals(content, 'i');
            const sB = getPosInArray(boldIntervals, sP) % 2 == 1;
            const sI = getPosInArray(italicIntervals, sP) % 2 == 1;

            action(v, sP, eP, content, boldIntervals, italicIntervals, sB, sI);

        }

        content.addEventListener("mouseup", (ev) => {
            //console.log("mouseup !");

            selectionChecker((v, sP, eP, content, boldIntervals, italicIntervals, sB, sI) => {
                if(eP-sP == 0){
                    document.getElementById("toolbox-bold").className = sB ? toolboxStyleSelected : toolboxStyle;
                    document.getElementById("toolbox-italic").className = sI ? toolboxStyleSelected : toolboxStyle;
                }else{
                    document.getElementById("toolbox-bold").className =
                        isAllStyled("b", "i", content.substring(sP, eP), sB, getPosInArray(boldIntervals, eP) %2 == 1) ?
                            toolboxStyleSelected : toolboxStyle;
                    document.getElementById("toolbox-italic").className =
                        isAllStyled("i", "b", content.substring(sP, eP), sI, getPosInArray(italicIntervals, eP) %2 == 1) ?
                            toolboxStyleSelected : toolboxStyle;
                }
            });
        });

        content.addEventListener("keyup", (ev) => {
            // left / up / right / down
            if(ev.keyCode == 37 || ev.keyCode == 38 || ev.keyCode == 39|| ev.keyCode == 40 ){
                selectionChecker((v, sP, eP, content, boldIntervals, italicIntervals, sB, sI) => {
                    document.getElementById("toolbox-bold").className = sB ? toolboxStyleSelected : toolboxStyle;
                    document.getElementById("toolbox-italic").className = sI ? toolboxStyleSelected : toolboxStyle;
                });
            }

        });

        content.addEventListener("input", (ev) => {
            selectionChecker((v, sP, eP, content, boldIntervals, italicIntervals, sB, sI) => {
                document.getElementById("toolbox-bold").className = sB ? toolboxStyleSelected : toolboxStyle;
                document.getElementById("toolbox-italic").className = sI ? toolboxStyleSelected : toolboxStyle;
            })
        })

        return content;
    }

    #createCardView(){
        const view = document.createElement("div");
        view.id = this.viewId();
        view.className = "note-view color-light-"+this.#color;

        this.addColorListener((note) => {
            view.className = "note-view color-light-"+note.#color;
        })

        view.onclick = (ev) => {
            updateToolBox(this);
        }

        return view;
    }

    save(){
        if(this.#id == -255){
            return;
        }

        let contentElement = document.getElementById(this.contentId()).innerHTML;
        this.setContent(htmlToContent(contentElement));


        const json = {
            id: this.#id,
            board: this.#board,
            title: this.#title,
            content: this.#content,
            created: ""+this.#created,
            lastModified: ""+this.#lastModified,
            position: this.#position,
            colorId: this.#color,

        }

        console.log("Trying to save note " + this.#id);
        console.log(json);
        fetchJson(saveNoteRoute, json, () => {
            console.log("Saved note at pos " + this.#position + " successfully");
        })
    }

    remove(){
        updateToolBox(dummyEmptyNote);

        fetchJson(deleteNoteRoute, {noteId: this.#id},res => {
            console.log("Removed note at pos " + this.#position + "from db successfully");
        })
    }
    viewId(){
        return "view-#" + this.#position;
    }

    titleId(){
        return "view-title-#" + this.#position;
    }

    contentId(){
        return "view-content-#" + this.#position;
    }

    addTitleListener(listener){
        this.#titleInputListeners.push(listener);
    }

    addContentInputListener(listener){
        this.#contentInputListeners.push(listener);
    }

    addColorListener(listener){
        this.#colorInputListeners.push(listener);
    }

}


function logout() {
    fetch(logoutRoute).then(res => {
        window.location.href = res.url;
    });
}

function getUsername(){
    fetchJson(userInfoRoute, {}, res => {
        document.getElementById("header-user-name").innerHTML = "Welcome " + res;
    })
}

function loadBoard() {
    const result = []

    fetchJson(getBoardsIdRoute, {}, res => {
        getNotes(res[0]);
    });
}

function getNotes(boardId) {

    let notesObj = [];

    fetchJson(notesRoute, {boardId: boardId}, notes => {
       for(let i = 0; i < notes.length; i++){
           notesObj.push(
               new Note(
                   notes[i].id,
                   notes[i].board,
                   notes[i].created,
                   notes[i].lastModified,
                   notes[i].position,
                   notes[i].colorId,
                   notes[i].title,
                   notes[i].content
               )
           )
       }

        displayGrid(notesObj, boardId);
    });
}

function createNote(boardId, position, callback){
    console.log("Creating a new note");
    fetchJson(createNoteRoute, {boardId: boardId, position: position}, (json) => {
        callback(json.id, json.created, json.lastModified);
    });
}

function displayGrid(notes, boardId){
    const grid = document.getElementById("board-container");
    let count = 0;
    for(let i = 0; i < notes.length; i++){
        let next = notes[i].position();
        while(count  < next){
            addEmptyView(grid, count, boardId)
            count++;
        }
        grid.appendChild(notes[i].createView());
        count++;
    }
    let max = count + 5;
    while(count < max){
        addEmptyView(grid, count, boardId)
        count++;
    }
}

function addEmptyView(grid, count, boardId){
    grid.appendChild(createEmptyView(count, boardId));
}

function createEmptyView(count, boardId){
    const emptyView = document.createElement("div");
    emptyView.className = "empty-view";
    emptyView.id = "view-#"+ count;

    const addIcon = document.createElement("img");
    addIcon.src = "/assets/images/plus_dark.png";
    addIcon.className = "add-icon"
    addIcon.id = "add-icon-#"+count;

    addIcon.style.opacity = "0.3";
    addIcon.hidden = true;

    emptyView.onmouseenter = (m) => {
        addIcon.hidden = false;
    }
    emptyView.onmouseleave = (m) => {
        addIcon.hidden = true;
    }

    emptyView.onclick = (ev) => {
        let position =  parseInt(ev.target.id.split('#')[1]);

        let note = new Note(-255, boardId, "undefined", "undefined", position, 0, "Title", "");
        const grid = document.getElementById("board-container");

        grid.removeChild(document.getElementById(note.viewId()));

        const next = document.getElementById("view-#"+(note.position()+1));
        if(next != null){
            grid.insertBefore(
                note.createView(),
                next
            );
        }
        else{
            grid.appendChild(
                note.createView()
            );
        }

        const childCount = grid.childNodes.length;
        const max = 6 + position - childCount;
        for(let i = 0; i < max; i++){
            addEmptyView(grid, childCount+i, boardId);
        }

        updateToolBox(note);

        createNote(boardId, position, (id, created, lastModified) => {
            note.setId(id);
            note.setCreated(created);
            note.setLastModified(lastModified);
        });
    }

    emptyView.appendChild(addIcon);

    return emptyView;
}

const dummyEmptyNote = new Note(-1, null, null, null, null, null, null, null);

function updateToolBox(note){
    document.getElementById("toolbox-close").onclick = () => {
        updateToolBox(null);
    }


    if(note == null){
        document.getElementById("board-toolbox").hidden = true;
        document.getElementById("board-screen").style.right = "0";
        return;
    }

    if(note == dummyEmptyNote){
        let toolboxChildren = document.getElementById("board-toolbox").children;
        for(let i = 0; i < toolboxChildren.length; i++){
            toolboxChildren.item(i).style.visibility = 'hidden';
        }

        document.getElementById("toolbox-close").style.visibility = 'visible';
        
    }else{
        let toolboxChildren = document.getElementById("board-toolbox").children;

        for(let i = 0; i < toolboxChildren.length; i++){
            toolboxChildren.item(i).style.visibility = 'visible';
        }

    }

    document.getElementById("board-toolbox").hidden = false;
    document.getElementById("board-screen").style.right = "250px";


    const titleInput = document.getElementById("selected-title");
    titleInput.value = note.title();

    titleInput.oninput = (ev) => {
        note.setTitle(titleInput.value);
    }

    for(let i = 0; i < 5; i++){
        document.getElementById("color-picker-#"+i).onclick = onColorClicked(note, i);
    }
    document.getElementById("toolbox-delete").onclick = (ev) => {
        document.getElementById("board-container").removeChild(document.getElementById(note.viewId()));

        document.getElementById("board-container").insertBefore(
            createEmptyView(note.position()),
            document.getElementById("view-#"+(note.position()+1))
        );

        note.remove();
    }

    document.getElementById("toolbox-bold").onclick = onBoldClicked(note);
    document.getElementById("toolbox-italic").onclick = onItalicClicked(note);


}

function onColorClicked(note, color) {
    return (ev) => {
        note.setColor(color);
        note.save();
        //console.log("Clicked for note : " + note.position());
    }
}



let data = [
    new Note(
        id = 0,
        board = 0,
        created = "2023-09-13 14:52:42.321208+02",
        last_modified = "2023-09-13 14:52:42.321208+02",
        position = 0,
        color = 0,
        title = "Quel joli titre !",
        content = "Une note vide **(juste un test)** sens mais tout de même bien remplie !",
    ),
    new Note(
        id = 1,
        board = 0,
        created = "2023-09-13 14:52:42.321208+02",
        last_modified = "2023-09-13 14:52:42.321208+02",
        position = 1,
        color = 1,
        title = "Le super titre !",
        content = "Il était une fois _____",
    ),
    new Note(
        id = 2,
        board = 0,
        created = "2023-09-13 14:52:42.321208+02",
        last_modified = "2023-09-13 14:52:42.321208+02",
        position = 5,
        color = 2,
        title = "Génial encore une note !",
        content = "SELECT * FROM users WHERE 1=1",
    ),
    new Note(
        id = 3,
        board = 0,
        created = "2023-09-13 14:52:42.321208+02",
        last_modified = "2023-09-13 14:52:42.321208+02",
        position = 12,
        color = 3,
        title = "WAAAAAAW",
        content = "WAAAAAAAAAAAAAAAAAAAAAAAAAAW",
    ),
    new Note(
        id = 4,
        board = 0,
        created = "2023-09-13 14:52:42.321208+02",
        last_modified = "2023-09-13 14:52:42.321208+02",
        position = 13,
        color = 4,
        title = "😮",
        content = ".",
    ),
    new Note(
        id = 5,
        board = 0,
        created = "2023-09-13 14:52:42.321208+02",
        last_modified = "2023-09-13 14:52:42.321208+02",
        position = 20,
        color = 0,
        title = "Boire : étape 1",
        content = "Pour commencer, munissez vous d'un verre. Si vous n'en possedez pas, n'importe quel récipient convexe imperméable fera l'affaire.\n" +
            "\n" +
            "Exemple de récipient adapté :\n" +
            "      - Une gourde\n" +
            "      - Une tasse\n" +
            "\n" +
            "Exemple de récipient non adapté :\n" +
            "      - Un filtre à café\n" +
            "      - Une nintendo switch"
    ),
    new Note(
        id = 6,
        board = 0,
        created = "2023-09-13 14:52:42.321208+02",
        last_modified = "2023-09-13 14:52:42.321208+02",
        position = 21,
        color = 1,
        title = "Boire : étape 2",
        content = "La deuxième étape consiste à remplir le récipient (cf étape 1)\n" +
            "\n" +
            "Choisissez le liquide ( potable ) de votre choix, puis versez le dans le verre jusqu'à 1 cm du rebord. \n" +
            "\n" +
            "Exemple de liquide adéquat :\n" +
            "      - De l'eau\n" +
            "      - Du jus d'orange\n" +
            "\n" +
            "Exemple de liquide non adéquat :\n" +
            "      - De l'alcool à bruler\n" +
            "      - De la colle UHU Twist\n" +
            "         and glue"
    ),
    new Note(
        id = 7,
        board = 0,
        created = "2023-09-13 14:52:42.321208+02",
        last_modified = "2023-09-13 14:52:42.321208+02",
        position = 22,
        color = 2,
        title = "Boire : étape 3",
        content = "Enfin, la troisième étape consiste à ammener le contenant près de la bouche afin d'y verser son contenu, puis de l'avaler.\n" +
            "\n" +
            "Attention tout de même à bien respecter les instructions suivantes : \n" +
            "      - Ne pas boire la tête à\n" +
            "         l'envers\n" +
            "      - Ne pas boire tout en se  \n" +
            "         brossant les dents\n" +
            "      - Ne pas renverser le \n" +
            "         récipient alors que vous \n" +
            "         fermez la bouche\n" +
            "\n",
    ),
    new Note(
        id = 8,
        board = 0,
        created = "2023-09-13 14:52:42.321208+02",
        last_modified = "2023-09-13 14:52:42.321208+02",
        position = 23,
        color = 3,
        title = "Boire : conclusion",
        content = "Les étapes précédemment détaillées peuvent vous sembler longue et fastidieuse, mais n'ayez craintes, rien n'est insurmontable avec de la rigueure et de l'amnégation. \n" +
            "\n" +
            "Entrainez vous chaque semaine et vous pourrez dans les plus brefs délais impressioner tous vos convives lors de votre prochain dinné !"
    )

];

function saveNote(jsNote){
    fetchJson(saveNoteRoute, jsNote, (res) => {
        console.log(res);
    })
}

loadBoard();
updateToolBox(null);
getUsername();
