const csrfToken = document.getElementById("csrf-token").value;

const indexRoute = document.getElementById("index-route").value;
const loginRoute = document.getElementById("login-route").value;
const validateRoute = document.getElementById("validate-route").value;

const boardRoute = document.getElementById("board-route").value;
const logoutRoute = document.getElementById("logout-route").value;
const userInfoRoute = document.getElementById("userinfo-route").value;
const notesRoute = document.getElementById("notes-route").value;

const registerRoute = document.getElementById("register-route").value;
const createUserRoute = document.getElementById("create-user-route").value;

const saveNoteRoute = document.getElementById("save-note-route").value;
const createNoteRoute = document.getElementById("create-note-route").value;
const deleteNoteRoute = document.getElementById("delete-note-route").value;

const getBoardsIdRoute = document.getElementById("get-boardsId-route").value;

function fetchJson(route, body= {}, callback = (res => res)) {
    fetch(route, {
        method: 'POST',
        headers: {'Content-Type': 'application/json', 'Csrf-Token': csrfToken },
        body: JSON.stringify(body)
    }).then(res => res.json()).then(json => {
        callback(json)
    });
}

function isValidEmail(email) {
    // Define the regular expression pattern
    const pattern = /^[A-Za-z0-9._+%-]+@[A-Za-z0-9.-]+\.[A-Za-z]+$/;

    // Use the test() method to check if the email matches the pattern
    return pattern.test(email);
}