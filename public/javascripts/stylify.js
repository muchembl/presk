const toolboxStyle = 'toolbox-styles';
const toolboxStyleSelected = 'toolbox-styles-selected';

function getSelectionPos(){
    let selection = document.getSelection();

    const view = selection.anchorNode.parentElement;
    let contentView;

    if(view.className == "note-content"){
        contentView = view;
    }else if(view.parentElement.className == "note-content"){
        contentView = view.parentElement;
    }else if(view.parentElement.parentElement.className == "note-content"){
        contentView = view.parentElement.parentElement;
    }else if(view.className.startsWith("note-view")){
        contentView = view.getElementsByClassName("note-content")[0];
    }


    //console.log(selection.getRangeAt(0).toString().length);

    const temp = document.createTextNode('\0');
    selection.getRangeAt(0).insertNode(temp);

    //console.log(view.className);

    let iHTML = "" + contentView.innerHTML;

    let startPos = iHTML.indexOf('\0');

    temp.parentElement.focus();

    if(selection.getRangeAt(0).toString().length == 1){
        document.getSelection().setPosition(temp, 0);
    }

    let pNode = temp.parentNode;
    temp.parentNode.removeChild(temp);
    pNode.normalize();

    let length = selection.getRangeAt(0).toString().length;
    // console.log("original selection : " + selection.getRangeAt(0).toString());

    let endPos = startPos;
    // console.log("function : getSelectionPosition ; length of the selection in text is : " + length);
    while(length !== 0){

        if(length === 1){
            // console.log("length 1 ; endPos : " + endPos + " ; char : " + iHTML.charAt(endPos));

        }
        if(iHTML.charAt(endPos+1) === "<"){
            //console.log("start with : " + iHTML.substring(endPos, endPos+4));

            if(iHTML.substring(endPos+1, endPos+4) === "<b>"){
                endPos += 3;
            }
            else if(iHTML.substring(endPos+1, endPos+5) === "</b>"){
                endPos += 4;
            }
            else if(iHTML.substring(endPos+1, endPos+4) === "<i>"){
                endPos += 3;
            }
            else if(iHTML.substring(endPos+1, endPos+5) === "</i>"){
                endPos += 4;
            }
            else if(iHTML.substring(endPos+1, endPos+5) === "<br>"){
                endPos += 4;
            }
            else if(iHTML.substring(endPos+1, endPos+6) === "</br>"){
                endPos += 5;
            }
        }else if(iHTML.substring(endPos+1, endPos+ 7) === "&nbsp;"){
            endPos += 6;
        }else {
            length -= 1;
            endPos++;
        }
    }

    // console.log("function : getSelectionPosition ; the selection started at : " + startPos + " and end at : " + endPos);
    return [contentView ,startPos, endPos];
}

function getPosInArray(array, value){
    let i = 0;
    while(i < array.length && array[i] < value) i++;
    return i;
}

function sanitize(string){

    // console.log("start sanitizing");

    const emptyBoldRegExp = /<b>\s<\/b>/gi;

    const brBoldRegExp = /<b>(<br>)+<\/b>/gi;
    const emptyItalicRegExp = /<i>\s<\/i>/gi;
    const brItalicRegExp = /<i>(<br>)+<\/i>/gi;

    let sanitized = string.replaceAll("<b></b>", '')
        .replaceAll(emptyBoldRegExp , ' ')
        .replaceAll(brBoldRegExp, '<br>')
        .replaceAll("<i></i>", '')
        .replaceAll(emptyItalicRegExp , ' ')
        .replaceAll(brItalicRegExp, '<br>');

    while(sanitized.endsWith("<br>")){
        sanitized = sanitized.substring(0, sanitized.length - 4);
    }

    const boldIntervals = intervals(sanitized, "b");
    const italicIntervals = intervals(sanitized, "i");

    if(boldIntervals.length == 0 || italicIntervals.length == 0) {
        // console.log("string after sanitized : " + sanitized);
        return sanitized;
    }

    let bIndex = 0;
    let iIndex = 0;
    while(bIndex != boldIntervals.length && iIndex != italicIntervals.length){

        let bOpenPos = boldIntervals[bIndex];
        let bClosePos = boldIntervals[bIndex +1];
        let iOpenPos = italicIntervals[iIndex];
        let iClosePos = italicIntervals[iIndex +1];

        if(bOpenPos < iOpenPos && bClosePos > iOpenPos && bClosePos < iClosePos){
            sanitized = sanitized.substring(0, iOpenPos)
                + "</b>" +
                sanitized.substring(iOpenPos, iOpenPos+ 3)
                + "<b>" +
                sanitized.substring(iOpenPos + 3);
        }else if(iOpenPos < bOpenPos && iClosePos > bOpenPos && iClosePos < bClosePos){
            sanitized = sanitized.substring(0, bOpenPos)
                + "</i>" +
                sanitized.substring(bOpenPos, bOpenPos+ 3)
                + "<i>" +
                sanitized.substring(bOpenPos + 3);
        }

        bIndex += 2;
        iIndex += 2;

    }

    // console.log("string after sanitized : " + sanitized);
    return sanitized;
}

function isAllStyled(tag, other, string, sS, eS){
    const oT = "<"+tag+">";
    const cT = "</"+tag+">";
    // console.log("original selection : " + string.replaceAll(" ", "!"));
    const styleCheckString = string.replaceAll(/\s*/gi, "")
        .replaceAll("<"+other+">", "")
        .replaceAll("</"+other+">","")
        .replaceAll(cT + oT, "");

    // console.log("styleCheckString : " + styleCheckString);
    const swb = styleCheckString.startsWith(oT);
    const ewb = styleCheckString.endsWith(cT);
    const containsOpen = styleCheckString.indexOf(oT) != -1;
    const containsClose = styleCheckString.indexOf(cT) != -1;

    return ( sS && eS && !containsOpen )
        || ( sS && !eS && !containsOpen && ewb )
        || ( !sS && eS && !containsClose && swb )
        || ( !sS && !eS && swb && ewb && (styleCheckString.substring(oT.length, styleCheckString.length - cT.length).indexOf(oT) == -1));
}

function stylify(tag, content, startPos, endPos){

    function removeTags(string, tOpen, tClosed){
        return string.replaceAll(tOpen, "").replaceAll(tClosed, "");
    }

    // Defines the appropriate constants according to "tag"
    const other = tag == "b" ? "i" : "b";
    const cTagOpen = "<" + tag + ">";
    const cTagClose = "</" + tag + ">";

    let string = "" + content;

    // Computes the positions of preexisting "tag" tags.
    let ints = intervals(string, tag);

    // Determines if start and end position are inside a "tag" tag.
    const isStartStyled = getPosInArray(ints, startPos) % 2 == 1;
    const isEndStyled = getPosInArray(ints, endPos) % 2 == 1;

    const allStyled = isAllStyled(tag, other, string.substring(startPos, endPos), isStartStyled, isEndStyled);
    // console.log("all styled : " + allStyled);
    const middleCleanString = removeTags(string.substring(startPos, endPos), cTagOpen, cTagClose);

    // console.log("isStartStyled : " + isStartStyled + " isEndStyled : " + isEndStyled + " allStyled : " + allStyled);

    if(isStartStyled && isEndStyled && allStyled){
        string = string.substring(0, startPos)
            + cTagClose
            + middleCleanString
            + cTagOpen
            + string.substring(endPos);
    }else if(isStartStyled && !isEndStyled && allStyled){
        string = string.substring(0, startPos)
            + cTagClose
            + middleCleanString
            + string.substring(endPos);
    }else if(isStartStyled && !isEndStyled && !allStyled){
        string = string.substring(0, startPos)
            + middleCleanString
            + cTagClose
            + string.substring(endPos);
    }else if(!isStartStyled && isEndStyled && allStyled){
        string = string.substring(0, startPos)
            + middleCleanString
            + cTagOpen
            + string.substring(endPos);
    }else if(!isStartStyled && isEndStyled && !allStyled){
        string = string.substring(0, startPos)
            + cTagOpen
            + middleCleanString
            + string.substring(endPos);
    }else if(!isStartStyled && !isEndStyled && !allStyled){
        string = string.substring(0, startPos)
            + cTagOpen
            + middleCleanString
            + cTagClose
            + string.substring(endPos);
    }else {
        string = string.substring(0, startPos)
            + middleCleanString
            + string.substring(endPos);
    }

    // console.log("string after styling : " + string);

    return string;

}

function boldify(content, startPos, endPos){
    return stylify("b", content, startPos, endPos);
}

function italify(content, posStart, posEnd){
    return stylify("i", content, posStart, posEnd);
}

function intervals(content ,tag){
    const opening = "<"+tag+">";
    const closing = "</"+tag+">";
    let str = ""+content;
    let indices = [];
    let i = 0;
    let j = 0;
    while(str.indexOf(opening) != -1){
        i = str.indexOf(opening);
        if(indices.length == 0){
            indices.push(i);
            str = str.substring(i+4);
        }else{
            indices.push(i+ 5 + indices[indices.length-1]);
            str = str.substring(i + 4);
        }

        j = str.indexOf(closing);
        if(j != -1){
            indices.push(j+ 4 +indices[indices.length-1]);
            str = str.substring(j+ 5);
        }else{
            return indices;
        }
    }

    return indices;
}

function onBoldClicked(note){
    return (ev) => {

        const boldButton = document.getElementById("toolbox-bold");
        const pos = getSelectionPos();
        const contentView = pos[0];
        const startPos = pos[1];
        const endPos = pos[2];

        boldButton.className
            = boldButton.className === toolboxStyle ? toolboxStyleSelected : toolboxStyle;

        if(startPos != endPos){
            contentView.innerHTML = sanitize(boldify(contentView.innerHTML, startPos, endPos));

        }else{
            // Unbolding
            if(boldButton.className == toolboxStyle){
                contentView.innerHTML = contentView.innerHTML.substring(0, startPos)
                    + "</b><z id='toremove'></z><b>"
                    + contentView.innerHTML.substring(startPos);

                let node = document.getElementById('toremove');
                document.getSelection().setPosition(node, 0);
                node.parentNode.removeChild(node);
            }
            // Bolding
            else {
                let node = document.createElement("b");

                document.getSelection().getRangeAt(0).insertNode(node);
                document.getSelection().setPosition(node, 0);
            }

        }

    }
}

function onItalicClicked(note){
    return (ev) => {

        const italicButton = document.getElementById("toolbox-italic");

        const pos = getSelectionPos();
        const contentView = pos[0];
        const startPos = pos[1];
        const endPos = pos[2];

        document.getElementById("toolbox-italic").className
            = document.getElementById("toolbox-italic").className === toolboxStyle ? toolboxStyleSelected : toolboxStyle;

        if(startPos != endPos){
            contentView.innerHTML = sanitize(italify(contentView.innerHTML, startPos, endPos));

        }else{
            // Unstyling
            if(italicButton.className == toolboxStyle){
                contentView.innerHTML = contentView.innerHTML.substring(0, startPos)
                    + "</i><z id='toremove'></z><i>"
                    + contentView.innerHTML.substring(startPos);

                let node = document.getElementById('toremove');
                document.getSelection().setPosition(node, 0);
                node.parentNode.removeChild(node);
            }
            // Styling
            else {
                let node = document.createElement("i");

                document.getSelection().getRangeAt(0).insertNode(node);
                document.getSelection().setPosition(node, 0);
            }
        }

    }
}

function htmlToContent(html){
    return html.replaceAll("<br>", "\n")
        .replaceAll( "<b>" , "**(")
        .replaceAll( "</b>", ")**")
        .replaceAll( "<i>" , "++(")
        .replaceAll( "</i>",")++");
}

function contentToHtml(content){
    return content.replaceAll("\n", "<br>")
        .replaceAll("**(", "<b>"  )
        .replaceAll(")**", "</b>" )
        .replaceAll("++(", "<i>"  )
        .replaceAll(")++", "</i>" );
}