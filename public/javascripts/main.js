function onLoginClicked(){
    $("#login-update").load(loginRoute, null, () => {
        document.getElementById("login-form").username.focus();
    })



    document.addEventListener("keydown", function(event) {
        if (event.key === "Enter") {
            validate();
        }
    });
}

function onRegisterClicked(){
    $("#login-update").load(registerRoute);

    document.addEventListener("keydown", function(event) {
        if (event.key === "Enter") {
            registerUser()
        }
    });
}

function validate() {
    const username = document.getElementById("username").value;
    const password = document.getElementById("password").value;

    fetchJson(validateRoute, { username, password }, res => {
        if (res) {
            $("#body").load(boardRoute);
        } else {
            document.getElementById("login-message").innerHTML = "";
            let error = document.createTextNode("Login failed. Please check your username and password");
            document.getElementById("login-form-return").hidden = true;
            document.getElementById("login-message").appendChild(error);
        }
    });
}

function registerMessage(message) {
    document.getElementById("register-message").innerHTML = "";
    document.getElementById("register-message").innerHTML = message;
}

function registerUser() {
    const new_username = document.getElementById("new_username").value;
    const new_email = document.getElementById("new_email").value;
    const new_password = document.getElementById("new_password").value;

    if (!isValidEmail(new_email)) {
        registerMessage("Please enter a valid email (name@domain.xx)");
    } else if (new_username === "" || new_password === "") {
        registerMessage("Please fill in a username and a password");
    } else{
        fetchJson(createUserRoute, { new_username, new_email, new_password }, res => {
            if (res) {
                $("#body").load(boardRoute);
            } else {
                let error = document.createTextNode("Register failed");
                document.getElementById("register-form-return").hidden = true;
                document.getElementById("register-message").appendChild(error);
            }
        });
    }
}