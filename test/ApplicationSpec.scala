import org.junit.runner._
import play.api.test._
import org.specs2._
import org.specs2.runner.JUnitRunner
import play.api.mvc

import scala.concurrent.Future
/**
 * Add your spec here.
 * You can mock out a whole application including requests, plugins etc.
 * For more information, consult the wiki.
 */
@RunWith(classOf[JUnitRunner])
class ApplicationSpec() extends PlaySpecification {

  "Application" should {

    "send 404 on a bad request" in new WithApplication {
      private val boum = route(app, FakeRequest(GET, "/boum")).get
      status(boum) must equalTo(NOT_FOUND)
    }

    "render the index page" in new WithApplication {
      private val home = route(app, FakeRequest(GET, "/")).get

      status(home) must equalTo(OK)
      contentAsString(home) must contain ("Your toughts in one place")
    }
  }
}
