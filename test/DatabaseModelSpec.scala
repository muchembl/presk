import akka.util.Timeout
import controllers.HomeController
import org.junit.runner._
import org.specs2.runner.JUnitRunner
import play.api.test._
import models.PreskDatabaseModel
import slick.jdbc.JdbcBackend

import scala.concurrent.ExecutionContext.global
import scala.concurrent.ExecutionContextExecutor
import scala.concurrent.duration.DurationInt
/**
 * Add your spec here.
 * You can mock out a whole application including requests, plugins etc.
 * For more information, consult the wiki.
 */
@RunWith(classOf[JUnitRunner])
class DatabaseModelSpec() extends PlaySpecification {

  implicit val ec: ExecutionContextExecutor = global
  val db = JdbcBackend.Database.forConfig("preskdb")
  val model = new PreskDatabaseModel(db)
  override implicit def defaultAwaitTimeout: Timeout = 20.seconds

  "The model" should {
      "be able to get a user id" in new WithApplication {
        await(model.getUserId("paul")) must beSome(7)
        await(model.getUserId("françis")) must beSome(9)
        await(model.getUserId("Barbidou")) must beSome(10)
        await(model.getUserId("I am not a valid username")) must beNone
      }

    "validate only users with the right password" in new WithApplication {
      await(model.validateUser("paul", "1234")) must beFalse
      await(model.validateUser("paul", "")) must beFalse
      await(model.validateUser("paul", "asdf")) must beTrue
      await(model.validateUser("", "asdf")) must beFalse
      await(model.validateUser("alfred", "Pariatur vitae voluptatem repudiandae quia vel labore qui. Ut nam expedita possimus et commodi neque laudantium. Quidem omnis sequi ipsam quas molestiae voluptate. Facilis et voluptate fugit quidem. Molestiae odio vero recusandae ut dolorum. Dignissimos consequuntur consectetur labore similique unde.\n\nSunt dolor tenetur qui ratione architecto nemo recusandae porro. Et in officia cum. Corrupti dolor soluta molestiae quia rerum.\n\nUt necessitatibus ducimus voluptatem id autem. Iusto quod non sed. Aliquid corporis quia delectus non necessitatibus ut dolor. Occaecati sed repellat voluptas iste est. Aut nulla animi est ex exercitationem maxime ut est. Et minus laborum porro nihil.\n\nTemporibus iusto ipsum quisquam repellat consequatur blanditiis. Aut accusantium autem possimus dolores placeat exercitationem eum consequatur. Asperiores ipsa aut soluta qui porro. Dolorem totam voluptas dolorum voluptatum nam nihil. Reiciendis facere harum deleniti vero.\n\nDoloremque dignissimos eligendi totam. Dolorum aut sed tempore illum qui provident debitis. Ab deleniti tenetur ut nesciunt similique ut vero omnis. Ex dolorem consequatur maxime autem.\n\nQuibusdam quis consequuntur velit repudiandae. Ut id sunt error quia deserunt ea voluptas. Non sint exercitationem dolor corrupti a. Sed repellendus optio qui.\n\nAt quo dolor deleniti. Consectetur ipsam consequatur excepturi hic aliquam. Non voluptatum enim molestiae. Et molestiae debitis ipsa quia voluptatibus blanditiis aperiam.")) must beTrue
    }

    "be able to check if a user exists" in new WithApplication() {
      await(model.userExists("paul")) must beTrue
      await(model.userExists("jean-pierre")) must beFalse
      await(model.userExists("")) must beFalse
      await(model.userExists("lorem ipsum")) must beFalse
      await(model.userExists("vivelafrance")) must beTrue
    }

    "be able to create a user, then delete it" in new WithApplication() {
      val username = "mockup username"
      val email = "mock@up.com"
      val password = "jkldflkklsdkjkdjk"
      await(model.userExists(username)) must beFalse
      await(model.createUser(username, email, password)) must beTrue
      await(model.userExists(username)) must beTrue
      await(model.validateUser(username, password)) must beTrue
      await(model.deleteUser(username)) must beTrue
      await(model.validateUser(username, password)) must beFalse
      await(model.userExists(username)) must beFalse
    }

  }
}
