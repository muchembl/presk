package models

object CodeGen extends App{
  slick.codegen.SourceCodeGenerator.run(
    "slick.jdbc.PostgresProfile",
    "org.postgresql.Driver",
    "jdbc:postgresql://localhost/presk?user=user&password=password",
    "/path/to/file/output",
    "models", None, None, true, false
  )
}
