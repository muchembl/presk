//package models
//
//import play.api.libs.json.{JsError, JsSuccess, JsValue, Json, Reads, Writes}
//
//import java.sql.Timestamp
//import scala.util.Try
//
//import Tables.NotesRow
//
//trait Serializers {
//  implicit val timestampReads: Reads[Timestamp] = (json: JsValue) => {
//    json.validate[String] match {
//      case JsSuccess(str, _) =>
//        Try(Timestamp.valueOf(str)).toOption match {
//          case Some(timestamp) => JsSuccess(timestamp)
//          case None => JsError("Invalid timestamp format")
//        }
//      case e@JsError(_) =>
//        println(e)
//        JsError("Error decoding Timestamp")
//    }
//  }
//
//  implicit val timestampWrites: Writes[Timestamp] = (timestamp: java.sql.Timestamp) => Json.toJson(timestamp.toString)
//
//  implicit val NewNoteDataReads: Reads[NewNoteData] = Json.reads[NewNoteData]
//
//  implicit val newNoteDataWrites: Writes[NewNoteData] = (newNote: NewNoteData) => Json.obj(
//    "id" -> newNote.id,
//    "created" -> newNote.created,
//    "lastModified" -> newNote.lastModified
//  )
//
//  implicit val notesRowReads: Reads[NotesRow] = Json.reads[NotesRow]
//
//  implicit val notesRowWrites: Writes[NotesRow] = (notesRow: NotesRow) => Json.obj(
//    "id" -> notesRow.id,
//    "board" -> notesRow.board,
//    "title" -> notesRow.title,
//    "content" -> notesRow.content,
//    "created" -> notesRow.created,
//    "lastModified" -> notesRow.lastModified,
//    "position" -> notesRow.position,
//    "colorId" -> notesRow.colorId
//  )
//}
