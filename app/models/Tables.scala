package models

import play.api.libs.json.{JsError, JsSuccess, JsValue, Json, Reads, Writes}

import java.sql.Timestamp
import scala.util.Try
// AUTO-GENERATED Slick data model
/** Stand-alone Slick data model for immediate use */
object Tables extends {
  val profile = slick.jdbc.PostgresProfile
} with Tables

/** Slick data model trait for extension, choice of backend or usage in the cake pattern. (Make sure to initialize this late.) */
trait Tables {
  val profile: slick.jdbc.JdbcProfile
  import profile.api._
  import slick.model.ForeignKeyAction
  // NOTE: GetResult mappers for plain SQL are only generated for tables where Slick knows how to map the types of all columns.
  import slick.jdbc.{GetResult => GR}

  /** DDL for all tables. Call .create to execute. */
  lazy val schema: profile.SchemaDescription = Boards.schema ++ Notes.schema ++ Users.schema

  implicit val timestampReads: Reads[Timestamp] = (json: JsValue) => {
    json.validate[String] match {
      case JsSuccess(str, _) =>
        Try(Timestamp.valueOf(str)).toOption match {
          case Some(timestamp) => JsSuccess(timestamp)
          case None => JsError("Invalid timestamp format")
        }
      case e@JsError(_) =>
        println(e)
        JsError("Error decoding Timestamp")
    }
  }

  implicit val timestampWrites: Writes[Timestamp] = (timestamp: java.sql.Timestamp) => Json.toJson(timestamp.toString)

  implicit val NewNoteDataReads: Reads[NewNoteData] = Json.reads[NewNoteData]

  implicit val newNoteDataWrites: Writes[NewNoteData] = (newNote: NewNoteData) => Json.obj(
    "id" -> newNote.id,
    "created" -> newNote.created,
    "lastModified" -> newNote.lastModified
  )


  implicit val notesRowReads: Reads[NotesRow] = Json.reads[NotesRow]

  implicit val notesRowWrites: Writes[NotesRow] = (notesRow: NotesRow) => Json.obj(
    "id" -> notesRow.id,
    "board" -> notesRow.board,
    "title" -> notesRow.title,
    "content" -> notesRow.content,
    "created" -> notesRow.created,
    "lastModified" -> notesRow.lastModified,
    "position" -> notesRow.position,
    "colorId" -> notesRow.colorId
  )
  @deprecated("Use .schema instead of .ddl", "3.0")
  def ddl = schema

  /** Entity class storing rows of table Boards
   *  @param id Database column id SqlType(serial), AutoInc, PrimaryKey
   *  @param owner Database column owner SqlType(int4)
   *  @param created Database column created SqlType(timestamptz)
   *  @param lastModified Database column last_modified SqlType(timestamptz) */
  case class BoardsRow(id: Int, owner: Int, created: java.sql.Timestamp, lastModified: java.sql.Timestamp)
  /** GetResult implicit for fetching BoardsRow objects using plain SQL queries */
  implicit def GetResultBoardsRow(implicit e0: GR[Int], e1: GR[java.sql.Timestamp]): GR[BoardsRow] = GR{
    prs => import prs._
    BoardsRow.tupled((<<[Int], <<[Int], <<[java.sql.Timestamp], <<[java.sql.Timestamp]))
  }
  /** Table description of table boards. Objects of this class serve as prototypes for rows in queries. */
  class Boards(_tableTag: Tag) extends profile.api.Table[BoardsRow](_tableTag, "boards") {
    def * = (id, owner, created, lastModified) <> (BoardsRow.tupled, BoardsRow.unapply)
    /** Maps whole row to an option. Useful for outer joins. */
    def ? = ((Rep.Some(id), Rep.Some(owner), Rep.Some(created), Rep.Some(lastModified))).shaped.<>({r=>import r._; _1.map(_=> BoardsRow.tupled((_1.get, _2.get, _3.get, _4.get)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))

    /** Database column id SqlType(serial), AutoInc, PrimaryKey */
    val id: Rep[Int] = column[Int]("id", O.AutoInc, O.PrimaryKey)
    /** Database column owner SqlType(int4) */
    val owner: Rep[Int] = column[Int]("owner")
    /** Database column created SqlType(timestamptz) */
    val created: Rep[java.sql.Timestamp] = column[java.sql.Timestamp]("created")
    /** Database column last_modified SqlType(timestamptz) */
    val lastModified: Rep[java.sql.Timestamp] = column[java.sql.Timestamp]("last_modified")

    /** Foreign key referencing Users (database name boards_owner_fkey) */
    lazy val usersFk = foreignKey("boards_owner_fkey", owner, Users)(r => r.id, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
  }
  /** Collection-like TableQuery object for table Boards */
  lazy val Boards = new TableQuery(tag => new Boards(tag))

  /** Entity class storing rows of table Notes
   *  @param id Database column id SqlType(serial), AutoInc, PrimaryKey
   *  @param board Database column board SqlType(int4)
   *  @param title Database column title SqlType(varchar), Length(50,true)
   *  @param content Database column content SqlType(text)
   *  @param created Database column created SqlType(timestamptz)
   *  @param lastModified Database column last_modified SqlType(timestamptz)
   *  @param position Database column position SqlType(int4)
   *  @param colorId Database column color_id SqlType(int2), Default(0) */
  case class NotesRow(id: Int, board: Int, title: String, content: String, created: java.sql.Timestamp, lastModified: java.sql.Timestamp, position: Int, colorId: Short = 0)
  /** GetResult implicit for fetching NotesRow objects using plain SQL queries */
  implicit def GetResultNotesRow(implicit e0: GR[Int], e1: GR[String], e2: GR[java.sql.Timestamp], e3: GR[Short]): GR[NotesRow] = GR{
    prs => import prs._
    NotesRow.tupled((<<[Int], <<[Int], <<[String], <<[String], <<[java.sql.Timestamp], <<[java.sql.Timestamp], <<[Int], <<[Short]))
  }
  /** Table description of table notes. Objects of this class serve as prototypes for rows in queries. */
  class Notes(_tableTag: Tag) extends profile.api.Table[NotesRow](_tableTag, "notes") {
    def * = (id, board, title, content, created, lastModified, position, colorId) <> (NotesRow.tupled, NotesRow.unapply)
    /** Maps whole row to an option. Useful for outer joins. */
    def ? = ((Rep.Some(id), Rep.Some(board), Rep.Some(title), Rep.Some(content), Rep.Some(created), Rep.Some(lastModified), Rep.Some(position), Rep.Some(colorId))).shaped.<>({r=>import r._; _1.map(_=> NotesRow.tupled((_1.get, _2.get, _3.get, _4.get, _5.get, _6.get, _7.get, _8.get)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))

    /** Database column id SqlType(serial), AutoInc, PrimaryKey */
    val id: Rep[Int] = column[Int]("id", O.AutoInc, O.PrimaryKey)
    /** Database column board SqlType(int4) */
    val board: Rep[Int] = column[Int]("board")
    /** Database column title SqlType(varchar), Length(50,true) */
    val title: Rep[String] = column[String]("title", O.Length(50,varying=true))
    /** Database column content SqlType(text) */
    val content: Rep[String] = column[String]("content")
    /** Database column created SqlType(timestamptz) */
    val created: Rep[java.sql.Timestamp] = column[java.sql.Timestamp]("created")
    /** Database column last_modified SqlType(timestamptz) */
    val lastModified: Rep[java.sql.Timestamp] = column[java.sql.Timestamp]("last_modified")
    /** Database column position SqlType(int4) */
    val position: Rep[Int] = column[Int]("position")
    /** Database column color_id SqlType(int2), Default(0) */
    val colorId: Rep[Short] = column[Short]("color_id", O.Default(0))

    /** Foreign key referencing Boards (database name notes_board_fkey) */
    lazy val boardsFk = foreignKey("notes_board_fkey", board, Boards)(r => r.id, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)

    /** Uniqueness Index over (board,position) (database name no_duplicate_note) */
    val index1 = index("no_duplicate_note", (board, position), unique=true)
  }
  /** Collection-like TableQuery object for table Notes */
  lazy val Notes = new TableQuery(tag => new Notes(tag))

  /** Entity class storing rows of table Users
   *  @param id Database column id SqlType(serial), AutoInc, PrimaryKey
   *  @param username Database column username SqlType(varchar), Length(50,true)
   *  @param email Database column email SqlType(varchar), Length(100,true)
   *  @param passwordHash Database column password_hash SqlType(varchar), Length(255,true) */
  case class UsersRow(id: Int, username: String, email: String, passwordHash: String)
  /** GetResult implicit for fetching UsersRow objects using plain SQL queries */
  implicit def GetResultUsersRow(implicit e0: GR[Int], e1: GR[String]): GR[UsersRow] = GR{
    prs => import prs._
    UsersRow.tupled((<<[Int], <<[String], <<[String], <<[String]))
  }
  /** Table description of table users. Objects of this class serve as prototypes for rows in queries. */
  class Users(_tableTag: Tag) extends profile.api.Table[UsersRow](_tableTag, "users") {
    def * = (id, username, email, passwordHash) <> (UsersRow.tupled, UsersRow.unapply)
    /** Maps whole row to an option. Useful for outer joins. */
    def ? = ((Rep.Some(id), Rep.Some(username), Rep.Some(email), Rep.Some(passwordHash))).shaped.<>({r=>import r._; _1.map(_=> UsersRow.tupled((_1.get, _2.get, _3.get, _4.get)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))

    /** Database column id SqlType(serial), AutoInc, PrimaryKey */
    val id: Rep[Int] = column[Int]("id", O.AutoInc, O.PrimaryKey)
    /** Database column username SqlType(varchar), Length(50,true) */
    val username: Rep[String] = column[String]("username", O.Length(50,varying=true))
    /** Database column email SqlType(varchar), Length(100,true) */
    val email: Rep[String] = column[String]("email", O.Length(100,varying=true))
    /** Database column password_hash SqlType(varchar), Length(255,true) */
    val passwordHash: Rep[String] = column[String]("password_hash", O.Length(255,varying=true))

    /** Uniqueness Index over (email) (database name users_email_key) */
    val index1 = index("users_email_key", email, unique=true)
    /** Uniqueness Index over (username) (database name users_username_key) */
    val index2 = index("users_username_key", username, unique=true)
  }
  /** Collection-like TableQuery object for table Users */
  lazy val Users = new TableQuery(tag => new Users(tag))
}
