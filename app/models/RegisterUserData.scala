package models

case class RegisterUserData(new_username: String, new_email: String, new_password: String)