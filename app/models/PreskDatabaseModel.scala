package models

import slick.jdbc.PostgresProfile.api._

import scala.concurrent.{ExecutionContext, Future}
import scala.concurrent.ExecutionContext.global
import models.Tables._
import org.mindrot.jbcrypt.BCrypt
import utils.Utils.printdbg

import java.sql.Timestamp
import java.util.Date
import scala.util.{Failure, Success}


class PreskDatabaseModel(db: Database)(implicit ec: ExecutionContext) {
  private def queryHandle[R, T](query: DBIOAction[R, NoStream, Nothing], onSuccess: R => T, onFailure: Throwable => T) =
    db.run(query).transform {
      _ match {
        case Success(s) => Success(onSuccess(s))
        case Failure(e) =>
          println(e)
          Success(onFailure(e))
      }
    }

  private def now = new Timestamp(new Date().getTime)

  def getUserId(username: String): Future[Option[Int]] = {
    val findUserQuery = Users.filter(userRow => userRow.username === username).result
    queryHandle[Seq[UsersRow], Option[Int]](findUserQuery, users => users.headOption.map(u => u.id), _ => None)
  }


  def userExists(username: String): Future[Boolean] = {
    val userExistsQuery = Users.filter(_.username === username).result
    queryHandle[Seq[UsersRow], Boolean](userExistsQuery, _.nonEmpty, _ => false)
  }

  /**
   * Tries to create a user in the database. If successful, returns the id of the new user in an option,
   * otherwise returns None.
   *
   * @param username the username of the new user
   * @param email the email of the new user
   * @param password the password of the new user
   * @return the id of the new user in an option
   */
  def createUser(username: String, email: String, password: String): Future[Option[Int]] = {
    //TODO see if there is not a slick command for not adding smth if it exists already
    userExists(username).flatMap{ userExists =>
      if (!userExists) {
        printdbg("Creating account for user \"" + username + "\".")
        val createUserQuery =
          (Users returning Users.map(_.id)) += UsersRow(-1, username, email, BCrypt.hashpw(password, BCrypt.gensalt()))
        queryHandle[Int, Option[Int]](createUserQuery, id => Some(id), _ => None)
      } else {
        printdbg("Can't create account for " + username + ", username is already taken.")
        Future.successful(None)
      }
    }
  }

  def validateUser(username: String, password: String): Future[Boolean] = {
    val findUserQuery = Users.filter(userRow => userRow.username === username).result

    queryHandle[Seq[UsersRow], Boolean](findUserQuery,
      userRows => userRows.exists(userRow => BCrypt.checkpw(password, userRow.passwordHash)),
      _ => false
    )
  }

  def deleteUser(username: String): Future[Boolean] = {
    getUserId(username).flatMap {
      case Some(id) =>
        val deleteUserQuery = Users.filter(_.id === id).delete
        queryHandle[Int, Boolean](deleteUserQuery, usersDeleted => usersDeleted > 0, _ => false)
      case None => Future.successful(true)
    }
  }

  def userOwnsBoard(username: String, boardId: Int): Future[Boolean] = {
    getBoardsId(username).map(boardsId => boardsId.contains(boardId))
  }

  def userOwnsNote(username: String, noteId: Int): Future[Boolean] = {
    getNote(noteId).flatMap {
      case Some(note) =>
        userOwnsBoard(username, note.board)
      case None => Future.successful(false)
    }
  }

  private def getNote(noteId: Int): Future[Option[NotesRow]] = {
    val noteQuery = Notes.filter(_.id === noteId).result
    queryHandle[Seq[NotesRow], Option[NotesRow]](noteQuery, notes => notes.headOption, _ => None)
  }

  def getNotes(boardId: Int): Future[Seq[NotesRow]] = {
    val notesQuery = Notes.filter(_.board === boardId).sortBy(_.position).result
    queryHandle[Seq[NotesRow], Seq[NotesRow]](notesQuery, notes => notes, _ => Seq.empty[NotesRow])
  }

  def saveNote(savedNote: NotesRow): Future[Boolean] = {
    // Define a query that checks if a note is present at the given position
    val existingNoteQuery = Notes
      .filter(note => note.board === savedNote.board && note.position === savedNote.position).result

    // Create an action that inserts or updates the note
    val notesQuery = existingNoteQuery.headOption.flatMap {
      case Some(_) => Notes.filter(_.id === savedNote.id).update(savedNote)
      case None => Notes += savedNote
    }

    queryHandle[Int, Boolean](notesQuery, notesAdded => notesAdded == 1, _ => false)
  }

  def deleteNote(noteId: Int): Future[Boolean] = {
    val deleteNoteQuery = Notes.filter(_.id === noteId).delete
    queryHandle[Int, Boolean](deleteNoteQuery, rowsDeleted => rowsDeleted == 1, _ => false)
  }

//  def isPositionFree(boardId: Int, position: Int): Future[Boolean] = {
//    val positionQuery = Notes.filter(note => (note.board === boardId)).filter(note => note.position ===  position).result
//
//    queryHandle[Seq[NotesRow], Boolean](positionQuery, notes => notes.isEmpty, _ => false)
//  }

  /**
   * Creates a new empty note on the specified board, at the specified position.
   *
   * @param boardId the id of the board where the note will be added
   * @param position the position where to add the note
   * @return the id of the newly created note
   */
  def newBlankNote(boardId: Int, position: Int): Future[Option[(Int, Timestamp)]] = {
    // TODO use isPositionFree
    val newNote = NotesRow(-1, boardId, "Title", "", now, now, position)
    val createNoteQuery = (Notes returning Notes.map(_.id)) += newNote
    val onSuccess: Int => Option[(Int, Timestamp)] = id => {
      Some((id, now))
    }
    val onFailure: Throwable => Option[(Int, Timestamp)] = e => {
      println(e)
      None
    }

    queryHandle[Int, Option[(Int, Timestamp)]](createNoteQuery, onSuccess, onFailure)
  }

  /**
   * Creates a new board for specified user, and returns the id of that board in an option.
   *
   * @param username the owner of the new board
   * @return the id of the new board in an option
   */
  def newBoard(username: String): Future[Option[Int]] = {
    getUserId(username).flatMap {
      case Some(ownerId) =>
        val addBoardQuery = (Boards returning Boards.map(_.id)) += BoardsRow(-1, ownerId, now, now)
        queryHandle[Int, Option[Int]](addBoardQuery, id => Some(id), _ => None)

      case None => Future.successful(None)
    }
  }

  /**
   * Returns the id(s) of the board(s) associated with a certain user.
   *
   * @param username the username of the owner of the boards
   * @return the id(s) of the board(s) associated with username
   */
  def getBoardsId(username: String): Future[Seq[Int]] = {
    getUserId(username).flatMap{
      case Some(id) =>
        val boardsIdQuery = Boards.filter(board => board.owner === id).result
        queryHandle[Seq[BoardsRow], Seq[Int]](boardsIdQuery, boards => boards.map(b => b.id), _ => Seq.empty[Int])

      case None => Future.successful(Seq.empty[Int])
    }

  }
}
