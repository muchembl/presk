package models

import java.sql.Timestamp

case class NewNoteData(id: Int, created: Timestamp, lastModified: Timestamp)
