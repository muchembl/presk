package controllers

import javax.inject._
import play.api.mvc._
import models.PreskDatabaseModel
import models.Tables._
import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfigProvider}
import play.api.libs.json._
import slick.jdbc.JdbcProfile
import java.sql.Timestamp
import java.util.Date
import scala.concurrent.{ExecutionContext, Future}
import utils.Utils._
import models.NewNoteData


/**
 * This controller creates an `Action` to handle HTTP requests to the
 * application's home page.
 */
@Singleton
class BoardController @Inject()(protected val dbConfigProvider: DatabaseConfigProvider, val cc: ControllerComponents)
                              (implicit ex: ExecutionContext) extends AbstractController(cc) with HasDatabaseConfigProvider[JdbcProfile] {

  private val model = new PreskDatabaseModel(db)
  private val emptySuccess = Ok(Json.toJson(Seq.empty[NotesRow]))
  private val okFalse = Ok(Json.toJson(false))
  def getNotes: Action[AnyContent] = Action.async { implicit request =>
    ifAuthenticatedFuture { username =>
      request.body.asJson.map { body =>
        (body \ "boardId").validate[Int] match {
          case JsSuccess(boardId, _) =>
            model.userOwnsBoard(username, boardId).flatMap {
              case true  => model.getNotes(boardId).map(notes => Ok(Json.toJson(notes)))
              case false => Future.successful(emptySuccess)
            }
          case JsError(_) => Future.successful(emptySuccess)
        }
      }.getOrElse(Future.successful(emptySuccess))
    }
  }
  def saveNote(): Action[AnyContent] = Action.async { implicit request =>
    printdbg("--- In saveNote ---")
    ifAuthenticatedFuture { username =>
      withJsonDoCase[NotesRow] { note =>
        printdbg("Trying to save note " + note.id)
        model.userOwnsBoard(username, note.board).flatMap {
          case true =>
            printdbg("User owns board")
            model.saveNote(note.copy(lastModified = new Timestamp(new Date().getTime))).map(res => {
              if (res) { printdbg("Note saved successfully") } else { printdbg("Error saving note") }
              Ok(Json.toJson(res))
            } )
          case false =>
            printdbg("User does not own board")
            Future.successful(okFalse)
        }
      }
    }
  }

  def createNote(): Action[AnyContent] = Action.async { implicit request =>
    printdbg("--- In createNote ---")
    ifAuthenticatedFuture { username =>
      request.body.asJson.map { body =>
        printdbg(body.toString())
        val boardId = body("boardId").asOpt[Int]
        val position = body("position").asOpt[Int]

        if (boardId.isDefined && position.isDefined) {
          model.userOwnsBoard(username, boardId.get).flatMap {
            case true =>
              model.newBlankNote(boardId.get, position.get).map {
                case Some((id, time)) => Ok(Json.toJson(NewNoteData(id, time, time)))
                case None => okFalse
              }
            case false => Future.successful(okFalse)
          }
        } else {
          // Happens if request body was malformed (I think so at least, we should try)
          Future.successful(okFalse)
        }
      }.getOrElse(Future.successful(okFalse))
    }
  }

  def deleteNote(): Action[AnyContent] = Action.async { implicit request =>
    ifAuthenticatedFuture { username =>
      request.body.asJson.map { body =>
        val noteId = body("noteId").asOpt[Int]

        if (noteId.isDefined) {
          model.userOwnsNote(username, noteId.get).flatMap {
            case true =>
              model.deleteNote(noteId.get).map(deleted => Ok(Json.toJson(deleted)))
            case false =>
              Future.successful(okFalse)
          }
        } else {
          Future.successful(okFalse)
        }
      }.getOrElse(Future.successful(okFalse))
    }
  }

  def getBoardsId: Action[AnyContent] = Action.async { implicit request =>
    ifAuthenticatedFuture { username =>
      printdbg("--- In getBoardsId ---")
      model.getBoardsId(username).map(res => Ok(Json.toJson(res)))
    }
  }

}
