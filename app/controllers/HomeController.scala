package controllers

import javax.inject._
import play.api._
import play.api.mvc._
import models._
import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfigProvider}
import play.api.libs.json._
import slick.jdbc.JdbcProfile
import slick.jdbc.PostgresProfile.api._

import scala.collection.Seq
import scala.concurrent.{ExecutionContext, Future}

import utils.Utils._

/**
 * This controller creates an `Action` to handle HTTP requests to the
 * application's home page.
 */
@Singleton
class HomeController @Inject()(protected val dbConfigProvider: DatabaseConfigProvider, val cc: ControllerComponents)
                              (implicit ex: ExecutionContext) extends AbstractController(cc) with HasDatabaseConfigProvider[JdbcProfile] {

  private val model = new PreskDatabaseModel(db)

  implicit val userDataReads: Reads[UserData] = Json.reads[UserData]
  def index(): Action[AnyContent] = Action { implicit request: Request[AnyContent] =>
    ifAuthenticated(_ => Ok(views.html.main(views.html.board())))
  }

  def favicon(file: String): Action[AnyContent] = Action { implicit request: Request[AnyContent] =>
    Redirect(routes.Assets.versioned(file))
  }

  def login(): Action[AnyContent] = Action { implicit request: Request[AnyContent] =>
    Ok(views.html.loginForm())
  }

  def register(): Action[AnyContent] = Action { implicit request: Request[AnyContent] =>
    Ok(views.html.registerForm())
  }

  def logout(): Action[AnyContent] = Action { implicit request: Request[AnyContent] =>
    Redirect(routes.HomeController.index).withNewSession
  }

  def board(): Action[AnyContent] = Action {implicit request =>
    ifAuthenticated(_ => Ok(views.html.board()))
  }

  def userinfo(): Action[AnyContent] = Action { implicit request =>
    ifAuthenticated(username => Ok(Json.toJson(username)))
  }

  def validate: Action[AnyContent] = Action.async { implicit request =>
    withJsonDoCase[UserData] { ud =>
      model.validateUser(ud.username, ud.password).map { userExists =>
        if (userExists) {
          Ok(Json.toJson(true))
            .withSession("username" -> ud.username, "csrfToken" -> play.filters.csrf.CSRF.getToken.map(_.value).getOrElse(""))
        } else {
          Ok(Json.toJson(false))
        }
      }
    }
  }

  implicit val registerUserDataReads: Reads[RegisterUserData] = Json.reads[RegisterUserData]

  def createUser: Action[AnyContent] = Action.async { implicit request: Request[AnyContent] =>
    withJsonDoCase[RegisterUserData] { rud =>
      model.createUser(rud.new_username, rud.new_email, rud.new_password).flatMap {
        case Some(id) =>
          printdbg("User successfully created with id: " + id)
          model.newBoard(rud.new_username).map { //TODO include userId in session to make less calls to the database
            case Some(id) =>
              printdbg("Board successfully created for user: " + rud.new_username + " with id " + id)
              Ok(Json.toJson(true)).withSession("username" -> rud.new_username, "csrfToken" -> play.filters.csrf.CSRF.getToken.map(_.value).getOrElse(""))
            case None =>
              printdbg("Board creation failed")
              Ok(Json.toJson(false))
          }
        case None =>
          Future.successful(Ok(Json.toJson(false)))
      }
    }
  }
}
