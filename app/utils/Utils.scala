package utils

import controllers.routes
import play.api.libs.json._
import play.api.mvc.Results.{Ok, Redirect}
import play.api.mvc.{AnyContent, Request, Result}

import scala.concurrent.Future

object Utils {
  def withJsonDoCase[A](f: A => Future[Result])(implicit request: Request[AnyContent], reads: Reads[A]): Future[Result] = {
    request.body.asJson.map { body =>
      Json.fromJson[A](body) match {
        case JsSuccess(res, path) => f(res)
        case e@JsError(message) => {
          Future.successful(Redirect(routes.HomeController.index))
        }
      }
    }.getOrElse(Future.successful(Redirect(routes.HomeController.index)))
  }

  def withJsonBodyPrim[A](f: A => Future[Result], lookup: JsLookupResult)(implicit request: Request[AnyContent], reads: Reads[A]): Future[Result] = {
    request.body.asJson.map { body =>
      lookup.validate[A] match {
        case JsSuccess(res, _) => f(res)
        case e@JsError(_) => Future.successful(Redirect(routes.HomeController.index))
      }
    }.getOrElse(Future.successful(Redirect(routes.HomeController.index)))
  }

  def ifAuthenticated(f: String => Result)(implicit request: Request[AnyContent]): Result = {
    val usernameOption = request.session.get("username")
    usernameOption.map { username =>
      f(username)
    }.getOrElse(Ok(views.html.index()))
  }

  def ifAuthenticatedFuture(f: String => Future[Result])(implicit request: Request[AnyContent]): Future[Result] = {
    request.session.get("username").map { username =>
      f(username)
    }.getOrElse(Future.successful(Ok(views.html.index())))
  }

  private val debugEnabled = true

  /**
   * Prints a message in the terminal if the global variable debugEnabled is set to true
   *
   * @param message: Int
   */
  def printdbg(message: String): Unit = {
    if (debugEnabled) {
      println("[DEBUG] " + message)
    }
  }
}
