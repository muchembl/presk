# Presk.
### *Your thoughts in one place*

## Presentation

Presk is a web-app allowing you to save all your ideas using a set of notes displayed on a grid. 
Write and organize your notes just like using a regular post-its !

Write everything you need to remember and then safely logout and to retrieve it next time from any device.

## Features
In Presk, you can:

✅ create a persistent account 

✅ add notes, write text in them 

✅ change their colors 

✅ edit text to make it bold and italic 

✅ delete notes 

and everything gets saved in a proper PostgreSQL database.

## Technology used

This app is built using the [play framework](https://playframework.com), a web-app development framework based on the **Scala** programming language. 

### Backend
Since this app uses the play framework, it is using **Scala** for its backend. As we were introduced to it in functional programming and paralellism and concurency courses before, we had previous knowledge about this language. 
Moreover, we find that it is an elegant language, very pleasant to work with, and less prone to errors than many other more imperative-style languages. 
For the database queries, we used an amazing library called [slick](https://scala-slick.org/), allowing us to handle the database almost as if the tables were regular Scala collections, and making it much safer against SQL injections.

The call to the database are all located in the file app/models/PreskDatabaseModel.scala.
To give you a better idea of what is happening, here is an example of a call

![img.png](img.png)

This function is in charge of getting all the notes associated to a certain board, given by boardId. The return type is a Future, since we are querying the database. To get our notes, we filter the Notes table, keeping only the notes whose board id match the one we are interested in. Then we sort them according to their position attribute, so that the front end receive them already in the right order. The queryHandle function is a utility function we wrote to facilitate the handling of database errors, as well as choosing what to return in every case.

This query is equivalent to ´SELECT * FROM notes WHERE boardId = boardId ORDER BY position´

### Database
For the database, we used Postgre SQL, as it is a standard open source DBMS and it easy to set up. We didn't use very sophisticated features, just simple tables with a few constraints to store our users, boards and notes.
The detailed description of the database can be found in the sql/createTables.sql file.

### Frontend
The front end is mostly written with pure javascript. This is not ideal, but as we are not familiar with the most modern tools like React, we figured that it would be sufficent for this project. 
One improvement if we were to start over would be to use [React](https://react.dev/) of [Scala.js](https://www.scala-js.org/) instead.

## Discussion
### Why Play ?
Even though the teacher encouraged us to use simple tools like P5.js (in-browser editing and rendering), we wanted to push this project a little bit furter, by creating a full stack regular web app, using a real database and not a simple file. 
As a result we have a real app that could technically be deployed pretty much as-is, even though it is probably not fully secure, and could benefit from more unit-testing (some was done on the database model, but not much on the controllers). 
This was also the occasion for us to discover some new very useful tools, which we are very likely to use again in the future.

### Difficulties encountered
It has been a little hard to start the project, as we had pretty much never used all these tools before, so the setup took us more than a week. 
One of the aspect we had the most troubles with was serializing and deserializing of the database objects, which is far from trivial. 
Also, the front end is surprisingly much more complex than the backend, mostly because of the way javascript works. The italic and bold part has been especially complex, and required quite extensive algorithmic reasoning to be completed.
